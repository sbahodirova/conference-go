from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    params = {
        "per_page": 1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    data = json.loads(response.content)

    try:
        return {"picture_url": data["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }

    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    geo_data = json.loads(response.content)
    print(geo_data)
    try:
        lat = geo_data[0]["lat"]
        lon = geo_data[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    weather_data = json.loads(response.content)

    try:
        return {
            "temp": weather_data["main"]["temp"],
            "description": weather_data["weather"][0]["description"],
        }

    except (KeyError, IndexError):
        return None
