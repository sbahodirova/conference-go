from common.json import ModelEncoder
from .models import Presentation, Status
from events.api_views import ConferenceListEncoder


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


class PresentationEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status",
    ]
    def get_extra_data(self, o):
        return {"status": o.status.name}
